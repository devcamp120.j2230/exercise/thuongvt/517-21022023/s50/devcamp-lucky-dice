
import { Component } from "react";
import img6 from "../assetment/image/6.png"
import img1 from "../assetment/image/1.png"
import img2 from "../assetment/image/2.png"
import img3 from "../assetment/image/3.png"
import img4 from "../assetment/image/4.png"
import img5 from "../assetment/image/5.png"

class Dice extends Component {
    constructor(props){
        super(props);
        
         this.state = ({
            imgShow: null,
            dice: 0
         })
    }

    btnNem = ()=>{
        let ranDom = Math.floor(Math.random()*6)+1
        console.log(ranDom)
        this.setState({
            dice: ranDom
        })

        if(this.state.dice===1){
            this.setState({
                imgShow: img1
            })
        }
        if(this.state.dice===2){
            this.setState({
                imgShow: img2
            })
        }
        if(this.state.dice===3){
            this.setState({
                imgShow: img3
            })
        }
        if(this.state.dice===4){
            this.setState({
                imgShow: img4
            })
        }
        if(this.state.dice===5){
            this.setState({
                imgShow: img5
            })
        }
        if(this.state.dice===6){
            this.setState({
                imgShow: img6
            })
        }
    }
    
    render(){
        return(
            <>
            <div>
                <>
                <img src={this.state.imgShow} alt="dice" style={{height:"200px", width:"200px",textAlign:'center'}}/>
                </>
            </div>
            <div className="row mt-3">
                <button className="btn btn-success form-control" onClick={this.btnNem}>Ném</button>
            </div>
            </>
        )
    }
}

export default Dice