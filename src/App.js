import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import Dice from "./component/dice";

function App() {
  return (
    <div className="container text-center mt-5">
      <div className="row justify-content-center">
        <div className="col-6">
          <Dice></Dice>
        </div>
      </div>
    </div>
  );
}

export default App;
